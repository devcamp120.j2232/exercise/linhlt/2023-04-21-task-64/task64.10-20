package com.devcamp.task64vouchersvalidate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task64VouchersValidateApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task64VouchersValidateApplication.class, args);
	}

}
