package com.devcamp.task64vouchersvalidate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task64vouchersvalidate.model.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long>{
    
}
